from django.apps import AppConfig


class CgapiConfig(AppConfig):
    name = 'cgapi'

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User as Django_user


class Game(models.Model):
    idgame = models.AutoField(db_column='idGame', primary_key=True)  # Field name made lowercase.
    nomgame = models.CharField(db_column='nomGame', max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'Game'


class Partie(models.Model):
    iduser1 = models.ForeignKey('User', models.DO_NOTHING, db_column='idUser1', related_name="user1")  # Field name made lowercase.
    iduser2 = models.ForeignKey('User', models.DO_NOTHING, db_column='idUser2', related_name="user2")  # Field name made lowercase.
    iduserwin = models.ForeignKey('User', models.DO_NOTHING, db_column='idUserWin')  # Field name made lowercase.
    evite = models.BooleanField()
    refus = models.BooleanField()
    lieu = models.CharField(max_length=50)
    idgame = models.ForeignKey(Game, models.DO_NOTHING, db_column='idGame', related_name="winner")  # Field name made lowercase.
    date = models.DateTimeField()

    class Meta:
        db_table = 'Partie'


class User(models.Model):
    iduser = models.AutoField(db_column='idUser', primary_key=True)  # Field name made lowercase.
    authuser = models.OneToOneField(Django_user, on_delete=models.CASCADE, db_column="authUser")
    scoreuser = models.IntegerField(db_column='scoreUser', default=0)  # Field name made lowercase.
    ageuser = models.IntegerField(db_column='ageUser', blank=True, null=True)  # Field name made lowercase.
    nbwinuser = models.IntegerField(db_column='nbwinUser', default=0)  # Field name made lowercase.
    nblostuser = models.IntegerField(db_column='nblostUser', default=0)  # Field name made lowercase.
    nbeviteuser = models.IntegerField(db_column='nbeviteUser', default=0)  # Field name made lowercase.

    friends = models.ForeignKey('User', db_column='friends', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        db_table = 'User'

    @staticmethod
    def create_user(username, pwd, mail, age=None):
        try:
            m_user = Django_user.objects.create_user(username, password=pwd, email=mail)
            User.objects.create(authuser=m_user, ageuser=age)
        except Exception as e:
            print(e)



